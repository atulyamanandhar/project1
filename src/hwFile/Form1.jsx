import React, { useState } from 'react'

const Form1 = () => {

    let [pname, setPname] = useState("")
    let [memail, setMemail] = useState("")
    let [semail, setSemail] = useState("")
    let [password, setPassword] = useState("")
    let [gender, setGender] =useState("")
    let [date, setDate] =useState("")
    let [isMarr, setIsMarried] =useState(true)
    let [role, setRole] =useState()
    let [des, setDes] =useState("")
    
    let [country, setCountry] =useState("")
    let [movie, setMovie] =useState("")
    

    let role1 = [
        { label: "Select Role", value: "", disabled: true },
        { label: "Admin", value: "admin" },
        { label: "Super Admin", value: "sadmin" },
        { label: "Customer", value: "customer" },
        { label: "Delivery Person", value: "dperson" },
      ];

      let genders = [
        { label: "Male", value: "male" },
        { label: "Female", value: "female" },
        { label: "Other", value: "other" },
      ];

  return (
    <div>
        <form onSubmit={(e) =>{
            e.preventDefault()
            console.log("Form is Submitted")
        }}>
            <label htmlFor='pname'>Product Name: </label>
            <input id='pname' value={pname} onChange={(e) =>{setPname(e.target.value)}} type='text'></input><br></br>

            <label htmlFor='memail'>Product Manager Email: </label>
            <input id='memail' value={memail} onChange={(e) =>{setMemail(e.target.value)}} type='email'></input><br></br>

            <label htmlFor='semail'>Staff Email: </label>
            <input id='semail' value={semail} onChange={(e) =>{setSemail(e.target.value)}} type='email'></input><br/>

            <label htmlFor='password'>Password: </label>
            <input id='password' value={password} onChange={(e) =>{setPassword(e.target.value)}} type='password'></input><br/>

            <label>Gender: </label>
            {genders.map((item, i) =>{
          return (
            <>
            <label htmlFor={item.value}>{item.label}</label>
            <input key={i} checked={gender === item.value} onChange={(e) =>{setGender(e.target.value)}} type='radio' id={item.value} value={item.value}></input>
            </>
            )
            })}<br/>

            <label htmlFor='date'>Product Mf Date: </label>
            <input id='date' value={date} onChange={(e) =>{setDate(e.target.value)}} type='date'></input><br/>

            <label htmlFor='ismar'>Manager IsMarried: </label>
            <input id='ismar' checked={isMarr} onChange={(e) =>{setIsMarried(e.target.checked)}} type='checkbox'></input><br/>


            <label htmlFor='role'>Role: </label>
            <select id='role' value={role} onChange={(e) =>{setRole(e.target.value)}}>
            {role1.map((item,i) =>{
            return(
            <option key={i} value={item.value} disabled={item.disabled}>{item.label}</option>
            )
            })}
        </select><br></br>
            
            <button type="submit">Send</button>

        </form>
    </div>
  )
}

export default Form1
